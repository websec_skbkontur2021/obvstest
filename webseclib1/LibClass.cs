﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace webseclib1
{
    public class LibClass
    {
        public bool Method1(User user)
        {
            var pwd = user.Password.Trim();
            if(!CheckShadow(pwd))
                throw new Exception("Invalid pwd");
            var process = Process.Start("usermod", $"--password {pwd}");
            if(process != null && process.ExitCode!=0)
                throw new Exception("Invalid ExitCode");
            return true;


        }

        private bool CheckShadow(string pwd)
        {
            var md5 = Regex.Match(pwd, "^\\$1\\$[\\w/\\.]{0,16}\\$[\\w/\\.]{22}$").Success;
            var sha256 = Regex.Match(pwd, "^\\$5\\$[\\w/\\.]{0,16}\\$[\\w/\\.]{43}$").Success;
            var sha512 = Regex.Match(pwd, "^\\$6\\$[\\w/\\.]{0,16}\\$[\\w/\\.]{86}$").Success;
            return md5 || sha256 || sha512;
        }


    }

    public class User
    {
        public string Password { get; set; }
    }
}